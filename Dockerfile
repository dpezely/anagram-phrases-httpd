# Dockerfile for compiling & running Rust-based Command-line program	 -*-conf-*-
# that uses dictionary word lists from host computer.

# Usage on local laptop/workstation:
# docker build -t anagram-phrases-httpd .
# Test:
# docker run -it --rm anagram-phrases-httpd anagram-phrases-httpd -q torchwood
# Production Run:
# docker run -it --rm -p 127.0.0.1:8080:8080 anagram-phrases-httpd
# (Container tag name is identical to the executable's filename.)

# We need to use the Rust build image, because
# we need the Rust compile and Cargo tooling
FROM rust:1.36 as build_step

# Create dummy project used only for dependencies
RUN USER=root cargo new --bin anagram-phrases-httpd
WORKDIR /anagram-phrases-httpd

# Copy *only* manifest files, for making an image with just dependencies
COPY ./Cargo.lock ./Cargo.lock
COPY ./Cargo.toml ./Cargo.toml

# Build dependencies
RUN cargo build --release --bin anagram-phrases-httpd

# Remove fake source code from dummy project
RUN rm src/*.rs
RUN rm /anagram-phrases-httpd/target/release/anagram-phrases-httpd

# Copy only the actual source code to avoid invalidating the cache
COPY ./src/ ./src/
# Ensure that the build process sees our actual source code as newer
# than previously compiled dummy version:
RUN touch src/*.rs

# This occurrence just needs to exist for passing a compiler check:
ENV LANG en_US

# Build again using actual source files intended for production:
RUN cargo build --release --bin anagram-phrases-httpd

# Prune for smaller final Docker image:
RUN strip /anagram-phrases-httpd/target/release/anagram-phrases-httpd

# Runtime image doesn't need compiler:
FROM bitnami/minideb as runtime

ENV DEBIAN_FRONTEND noninteractive
# Ensure current patches with dist-upgrade:
RUN apt-get update && apt-get dist-upgrade -y
RUN apt-get install -y nginx wamerican-huge wbritish-huge wcanadian-huge
RUN apt-get clean
RUN rm -rf /var/lib/apt/lists/*

# Omit unnecessarily copying to /etc/nginx/sites-available/
COPY nginx-anagrams.conf /etc/nginx/sites-enabled/
RUN nginx -t

RUN mkdir /home/anagrams
WORKDIR /home/anagrams
COPY htdocs/ htdocs/

COPY --from=build_step \
  /anagram-phrases-httpd/target/release/anagram-phrases-httpd \
  /usr/local/bin/

# Might be useful to those wanting to change the dictionary word list used:
RUN ls -l /usr/share/dict/

# For Nginx:
EXPOSE 8080
# For debugging convenience, you might also want to expose anagram port:
# EXPLOSE 8081

# FIXME: which dictionary word list file do you want? and check LANG too!
ENV LANG en_CA.UTF-8
CMD nginx && anagram-phrases-httpd --listen=0.0.0.0:8081 \
    --dict=/usr/share/dict/canadian-english-huge
