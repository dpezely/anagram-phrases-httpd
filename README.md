Anagram-phrases Web Service
===========================

Phrase-based anagram solver using prime number factorization as web service:

This uses [anagram-phrases](https://github.com/dpezely/anagram-phrases) as a
library, which was written by the same author.

Heavy lifting for anagrams gets performed by the library.

This is a web service API wrapper using the [Actix-web](http://actix.rs/)
v1.0 framework in Rust.  It was intended to be minimal while making proper
use of Actix-web, since their API changed significantly compared to their
0.7 version and earlier.  The only real cheats here for simplicity: 1)
omission of real logging and uses STDOUT instead; 2) lack of any
configuration file and uses only command-line options; 3) HTTP keep-alive
timeout is a constant value.

There is only one request type possible, but multiple options are supported.

Clients of this API make HTTP calls that involve a "long-polling" request.
This means that there may be potentially several minutes before a response.

Only one natural language per runtime instance is supported, but there may
be multiple concurrent running instances on different IP ports within the
same host or container.

## Basic Usage

**This has only be tested on Linux**

Compile using [Rust](http://rust-lang.org/) Edition 2018 or newer, which is
available for BSD Unix, Linux, macOS, Windows and other operating systems:

    cargo build --release

Install its one executable somewhere convenient:

    sudo cp target/release/anagram-phrases-httpd /usr/local/bin/

Usage on Debian/Ubuntu and similar flavours of Linux:

    anagram-phrases-httpd --help

Perform quick test from command-line and exit immediately:

    anagram-phrases-httpd -q "word or phrase"
    
Note that such a test is _less_ optimal than using the command-line version
of the library dependency,
[anagram-phrases](https://github.com/dpezely/anagram-phrases) directly.  The
dictionary here gets loaded entirely and cached upon start of the web
service.  Then, per query, it scans and copies references from that cached
dictionary word list.  With those caveats, such tests are useful from an
Operations/dev-ops perspective, because it assists ruling-out certain
classes of issues, such as problems with data or permissions.

Listen as web service:

    anagram-phrases-httpd -l 127.0.0.1:8080

Logging messages will appear via STDOUT.

Wait for "Listening..." before making HTTP requests.

Queries may be performed using `wget` or `curl` or a full featured web
browser:

    http://127.0.0.1:8000/api/v1/anagrams?q=multiple+word+phrase&lang=EN

Results will be UTF-8 text data formatted as JSON.

## Advanced Usage

For stream-processing of the response, a count of single-word and
multiple-word phrase anagrams are given first.

The first 50-60 bytes of the response would need to be *processed* as UTF-8
characters before being *parsed* as JSON to retain the option of aborting
early if the number of entries is deemed too large.

## Containers

See [Makefile](./Makefile) and [Dockerfile](./Dockerfile) for details.

### Compile:

    make docker-image

The above command uses a *chained* Dockerfile (multiple FROM commands in
same file) to:

1. Build just Rust dependencies and cache that as a layer
2. Compile our release executable, which runs almost instantly on subsequent
   builds with same dependencies because of caching
3. copy just the executable to another small-footprint image

### Confirm: 

    make docker-test

The above command demonstrates that it works by running inside the container
but using our special "perform the specified query and exit immediately"
option.

### Run:

    make docker-run

There will be a URL displayed just above the "docker run..." command, but be
sure to put it within quotes if using `curl` or `wget` or other command-line
browser.

### Miscellaneous:

While debugging the code or deployment configuration, it's sometimes nice
having a shell into the container:

    make docker-shell
    
The command above bypasses the default command (`CMD nginx && anagrams`...)
and gives shell access.

## Advanced Deployments

Consider fronting with Nginx or comparable HTTP server for HTTPS/TLS
termination and rate-limiting.

See <https://www.nginx.com/blog/rate-limiting-nginx/> and
<https://nginx.org/en/docs/http/ngx_http_limit_req_module.html#limit_req_zone>

## Demo Ajax Web Page

As a *convenience* and for demonstration purposes only, the following items
are included within this repo:

- htdocs/anagrams.js -- minimal JavaScript without frameworks:
  + For form-validation
  + Async requests to Anagrams API
  + Rending from JSON to something visually practical
- htdocs/anagrams.html -- minimal HTML5:
  + Basic web Form
  + Area for dynamically rendering results in same page
  + Degrades *gracefully* when JavaScript is disabled, but it also loses
    rending of JSON, which arrives compact without white-space
- htdocs/anagrams.css -- minimal style-sheet
  + Responsive styling for big and small screens, alike
  + Uses BEM naming conventions

This is just enough front-end work to exercise the server-side Anagrams API
through asynchronous communications initiated from browser-side JavaScript.

## API

### Version 1:

**Ping**

Simple ping (health-check) to confirm that the service is running:

    /api/v1/ping
    
The ping returns HTTP 204/No-Content status.

**Anagrams**

Use proper URL encoding, so Space (0x20) characters should become Plus (`+`,
0x2b) within a phrase containing multiple words:

    /api/v1/anagrams?q=multiple+word+phrase&lang=EN&max=5
    
Query-string parameters are the "long" parameter names from the command-line
version of the library, but not all of those options are supported for HTTP
requests.

Required API parameter:

- `q` -- Query value is a multiple word phrase or single word protected by
  URL Encoding.

Optional API parameters:

- `lang` -- UPPER-CASE two letter ISO code representing a language such as
  `AL`=Albanian, `EN`=English, `FR`=French, `GR`=Greek, `IT`=Italian, etc.
  + Value is case-sensitive and must be upper-case!
  + This enables language-specific optimizations such as omitting all but a
    few specific single letter words and omitting proper names.
  + Omitting this parameter simply omits any optimization for potentially
    "noisy" results.
  + _Sorry, only English is effectively supported in the initial version._ (FIXME)
- `max` -- Number of words in longest phrase within results
  + Default value computed to be one more than number of words from input
    phrase.
  + Minimum number is 3, and lower values are ignored.
- `short` -- If supplied, must be `true` or `false` indicating whether or
  not to omit single letter words within dictionary.  (Consider using `lang`
  instead, which preserves "a" for English and "y" for Spanish, for
  example.)
- `upcase` -- If supplied, must be `true` or `false` indicating whether or
  not to omit words starting with an upper-case letter within dictionary,
  which would indicate a proper noun.  (Consider using `lang` instead, which
  preserves "I" for English, for example.)

While only the `q` parameter is required, including `lang` is useful for
pruning results.

For support of languages beyond English, check the status of library upon
which this package depends:
[anagram-phrases/CHANGES.md](https://github.com/dpezely/anagram-phrases/blob/master/CHANGES.md).
