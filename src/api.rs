//! This implements the principal method providing an HTTP API wrapper
//! around [anagram-phrases](https://github.com/dpezely/anagram-phrases).
//! While that crate has a command-line interface as a build option,
//! this connects into its core library.
//!
//! This offers a single request API end-point via public `search()`
//! method and single response as String for JSON via `serde`.
//!
//! For the HTTP layer, see `crate::httpd`.
//!
//! Unlike the command-line version of the external library which
//! loads its dictionary word list on-demand, this pre-loads and
//! caches potentially multiple dictionaries in advance.
//!
//! Of the resulting JSON structure:
//!
//! Elements of the outer vector represents one resulting phrase to
//! resolve the anagram, and the inner vector contains individual
//! words that themselves are anagrams of each other.  (In many cases,
//! a phrase is represented by a vector of single words, where each
//! word appears to be wrapped in a seemingly useless vector.)

use anagram_phrases::error::ErrorKind;
use anagram_phrases::languages::{self, SHORT, UPCASE};
use anagram_phrases::primes::Map;
use anagram_phrases::search::{self, Candidate};
use anagram_phrases::session::Session;
use std::collections::BTreeMap;
use serde::Serialize;
use serde_json;

/// Although JSON specifies that the order of its fields may be
/// presented in any order, serde_json 1.0 maintains order within the
/// struct.
///
/// For accommodating stream-processing by the receiving side,
/// mentioning each count before principal data payload gives an
/// opportunity for the receiving side to inspect the first several
/// bytes and abort if data volume deemed too large.
#[derive(Serialize, Debug)]
#[serde(rename_all="kebab-case")]
struct SearchResults<'a> {
    single_word_count: usize,
    multiple_word_count: usize,

    single_words: Vec<String>,
    multiple_words: Candidate<'a>,
}

/// Initiate the actual search based upon input parameters within
/// `session` and utilizing the pre-loaded `cache` dictionary word
/// list.  A per-session subset of `cache` gets created and discarded
/// while processing the search request.
/// Resulting String contains JSON as rendered by `serde_json` or JSON
/// value for null upon serialization error.
pub fn search(session: &Session, cache: &Map) -> Result<String, ErrorKind> {
    let mut wordlist: Vec<String> = vec![];
    let mut map: Map = BTreeMap::new();
    session_filter(&mut wordlist, &mut map, &cache, &session);
    let phrases = search::brute_force(&session.primes_product, &map,
                                      session.max_phrase_words);
    let results = SearchResults{single_word_count: wordlist.len(),
                                multiple_word_count: phrases.0.len(),
                                single_words: wordlist,
                                multiple_words: phrases};
    match serde_json::to_string(&results) {
        Ok(json) => Ok(json),
        Err(e) => {
            println!("serde error={:?} input-phrase=\"{}\"",
                     &e, &session.input_phrase.join(" "));
            Err(ErrorKind::SerializationError)
        }
    }
}

/// Process pre-loaded dictionary word list within `map` for the
/// current `session`, such that a subset get generated and stored
/// within `wordlist` Vector and `new_map` BTreeMap for significantly
/// reducing the search space of only this one session's query.
///
/// Reject dictionary words based upon various criteria: 1) too long
/// to possibly match; 2) containing characters other than those from
/// the input pattern; 3) words where their product is greater than
/// that of the input phrase.
///
/// Param `cache` should be populated by the calling scope with
/// unfiltered dictionary word list.
/// Params `wordlist` is set of single word matches, and `map` is a
/// tree containing dictionary words selected after initial filtering.
/// Other parameters are same as their namesakes from
/// `primes::filter_word()`.
/// SIDE-EFFECTS: `wordlist` and `new_map` will likely be updated.
/// See also: `crate::anagrams::load_wordlist()` for command-line version.
// This implementation essentially combines yet differs from both
// `anagram-phrases::primes::load_wordlist()` and
// `anagram-phrases::primes::filter_word()` because `cache` contains
// the product of each word from initial loading of the dictionary;
// otherwise, overall logic here should remain equivalent.
pub fn session_filter(wordlist: &mut Vec<String>, map: &mut Map,
                      cache: &Map, session: &Session) {
    let empty: Vec<&str> = vec![];
    let short_words = SHORT.get(&session.lang).unwrap_or(&empty);
    let upcase_words = UPCASE.get(&session.lang).unwrap_or(&empty);
    for (product, words) in cache.iter() {
        if *product > session.primes_product {
            continue    // too big, therefore cannot be a factor
        }
        if session.input_phrase.iter().any(|&x| words.iter().any(|w| w==x)) {
            continue    // filter words from input phrase
        }
        for word in words {
            if languages::filter(&word, &short_words, &upcase_words,
                                 session.skip_short, session.skip_upcase) {
                continue
            }
            if *product == session.primes_product { // single word match
                wordlist.push(word.to_string());
            } else {
                // Store remaining words in look-up table:
                map.entry(product.clone())
                    .or_insert_with(|| Vec::with_capacity(1))
                    .push(word.to_string());
            }
        }
    }
}
