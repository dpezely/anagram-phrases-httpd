//! This module is a thin wrapper for facilitating HTTP requests.
//!
//! The external library performs the heavy lifting regarding
//! searching for anagrams:
//! [anagram-phrases](https://github.com/dpezely/anagram-phrases).

use actix_web::{middleware, web, App, HttpResponse, HttpServer, Result, Error};
use futures::future::{ok, Future};
use serde::Deserialize;

use anagram_phrases::error::ErrorKind;
use anagram_phrases::languages::{Language, Region};
use anagram_phrases::primes::Map;
use anagram_phrases::session::Session;

use crate::api;

/// HTTP Keep-Alive.  Unique primes give clues in cascade of timeouts!
const KEEP_ALIVE_SECONDS: usize = 181;

/// HTTP Content-Type for JSON data.
const CONTENT_TYPE: &str = "application/json; charset=utf-8";

/// Actix-web uses Serde and parses Query-String parameters based upon
/// this struct, and it's a subset of `anagram_phrases::main::Options`.
#[derive(Deserialize, Debug)]
struct AnagramRequest {
    lang: Option<Language>,
    region: Option<Region>,

    #[serde(rename="max")]
    max_phrase_words: Option<usize>,

    #[serde(rename="upcase")]
    skip_upcase: Option<bool>,

    #[serde(rename="short")]
    skip_short: Option<bool>,

    #[serde(rename="q")]
    input_string: String,
}

/// Application State to be treated as immutable after initialization
/// and distinctly different than `Session` state
pub struct AppState {
    pub cache: Map,
}

pub fn start_server(listen_ip_addr: &str, cache: Map) -> Result<(), ErrorKind> {
    // https://docs.rs/actix-web/1.0.2/actix_web/web/struct.Data.html
    let state = web::Data::new(AppState{cache});
    println!("Listening...");
    HttpServer::new(move || {
        App::new()
            // https://actix.rs/docs/application/#state
            .register_data(state.clone())
            .wrap(middleware::Logger::default())
            .service(web::scope("/api/v1")
                     //.guard(guard::Header("Accept-Language", "en"))
                     .route("/ping", web::to_async(HttpResponse::NoContent))
                     .route("/anagrams", web::to_async(query)))
    })
        .bind(listen_ip_addr)?
        .keep_alive(KEEP_ALIVE_SECONDS)
        .run()?;         // Or use .start() for single-threaded
    Ok(())
}

// https://actix.rs/docs/handlers/#async-handlers
fn query(state: web::Data<AppState>,
         params: web::Query<AnagramRequest>) -> Box<dyn Future<Item = HttpResponse,
                                                               Error = Error>> {
    let lang = if let Some(lang) = &params.lang {&lang} else {&Language::Any};
    println!("lang={} query=\"{}\"", &lang, &params.input_string);
    let session =
        match Session::start(&lang, vec![], false,
                             params.max_phrase_words.unwrap_or(0),
                             params.skip_upcase.unwrap_or(false),
                             params.skip_short.unwrap_or(false),
                             false, &params.input_string) {
            Ok(session) => session,
            Err(e) => {
                println!("Unable to create session: {:?}", e);
                let resp = HttpResponse::BadRequest().body(e.to_string());
                return Box::new(ok::<_,Error>(resp));
            }
        };
    let resp = 
        match api::search(&session, &state.cache) {
            Ok(json) => {
                HttpResponse::Ok()
                    .content_type(CONTENT_TYPE)
                    .body(json)
            }
            Err(e) => {
                println!("serde error={:?} input=\"{}\"",
                         e, &params.input_string);
                HttpResponse::InternalServerError().body("Oops!")
            }
        };
    Box::new(ok::<_,Error>(resp))
}
