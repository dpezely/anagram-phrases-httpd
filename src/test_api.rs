use std::collections::BTreeMap;

use crate::api::*;
use anagram_phrases::languages::Language;
use anagram_phrases::primes::{self, Map};
use anagram_phrases::session::Session;

/// Words from input-phrase should be filtered (eliminated) from
/// per-session `map` despite being within the pre-loaded `cache` of
/// dictionary word list.
#[test]
fn test_session_filter() {
    const INPUT_PHRASE: &'static str = "reject these words";
    let mut cache: Map = BTreeMap::new();
    for word in INPUT_PHRASE.split(" ") {
        let essential = primes::essential_chars(&word);
        let primes = primes::primes(&essential).unwrap();
        let product = primes::primes_product(&primes).unwrap();
        cache.insert(product, vec![word.to_string()]);
    }
    let mut map: Map = BTreeMap::new();
    let mut wordlist: Vec<String> = vec![];
    let session = Session::start(&Language::Any, vec![], false, 9,
                                 false, false, false, INPUT_PHRASE).unwrap();
    session_filter(&mut wordlist, &mut map, &cache, &session);
    assert_eq!(cache.len(), 3);
    assert_eq!(map.len(), 0);
}
