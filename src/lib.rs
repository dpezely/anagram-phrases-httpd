extern crate actix_web;
extern crate anagram_phrases;
extern crate clap;
extern crate serde;
extern crate serde_json;

pub mod api;
pub mod httpd;
#[cfg(test)]
mod test_api;
