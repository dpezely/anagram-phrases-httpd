use anagram_phrases::error::ErrorKind;
use anagram_phrases::languages;
use anagram_phrases::primes::{self, Map};
use anagram_phrases::session::Session;
use clap::App;
use std::collections::BTreeMap;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

mod api;
mod httpd;

/// Default IP address and port number pair on which to listen
/// for HTTP requests
const LISTEN_IP_ADDR: &str = "127.0.0.1:8080";

fn main() -> Result<(), ErrorKind> {
    let yaml = clap::load_yaml!("app-args.en.yaml");
    let matches = App::from_yaml(yaml)
        .name(clap::crate_name!())
        .version(clap::crate_version!())
        .about(clap::crate_description!())
        .max_term_width(80)
        .get_matches();
    let mut cache: Map = BTreeMap::new();
    let (language, _region) =
        languages::parse_lang(matches.value_of("lang")
                             .unwrap_or_else(|| env!["LANG"]));
    if let Some(paths) = matches.values_of("dictionaries") {
        for filepath in paths {
            preload_wordlist(&mut cache, filepath)?;
        }
    } else {
        // FIXME: load specific file based upon value of env!["LANG"]
        preload_wordlist(&mut cache, "/usr/share/dict/words")?;
    }
    if let Some(query) = matches.value_of("query") {
        let session = Session::start(&language, vec![], false, 0, false, false,
                                     false, query)?;
        let json = api::search(&session, &cache)?;
        println!("{}", json);
    } else if let Some(ip_addr) = matches.value_of("listen") {
        httpd::start_server(ip_addr, cache)?;
    } else {
        httpd::start_server(LISTEN_IP_ADDR, cache)?;
    }
    Ok(())
}

/// Unlike its counterpart within the core library,
/// `crate::anagram-phrases`, which filters while loading, this
/// version loads the entire dictionary word list.  It's intended that
/// this function gets invoked once per natural language supported.
/// It is up to the calling scope to accommodate any containment and
/// isolation of those languages.  Also, this *requires* dictionary
/// word list files use UTF-8 encoding; see GNU/Unix utility, `iconv(1)`,
/// for internationalization-conversion.
///
/// Param `map` is a tree containing entire dictionary word list from
/// file specified by `filepath`.  Other parameters are same as their
/// namesakes from `crate::primes::filter_word()`.
/// SIDE-EFFECTS: `map` will be updated.
fn preload_wordlist(map: &mut Map, filepath: &str) -> Result<(), ErrorKind> {
    let f = File::open(filepath)?;
    let mut f = BufReader::new(f);
    let mut word = String::new();
    let mut previous = String::new();
    let mut i = 0;
    loop {
        i += 1;
        word.clear();
        match f.read_line(&mut word) {
            Ok(0) => break,     // End of file (EOF)
            Ok(_n) => {
                word = word.trim().to_string();
                if word.is_empty() {
                    continue
                }
                if word == previous { // some dictionaries contain duplicates
                    continue
                }
                let essential = primes::essential_chars(&word);
                let primes = primes::primes(&essential)?;
                let product = primes::primes_product(&primes)?;
                // FIXME: utilize PUSH-NEW semantics
                map.entry(product)
                    .or_insert_with(|| Vec::with_capacity(1))
                    .push(word.to_string());

                std::mem::swap(&mut previous, &mut word);
            }
            Err(e) => {
                println!("File error: line={} {:?}", i, e);
                break
            }
        }
    }
    println!("Cached dictionary word list: \
              path={} lines={}, filtered-entries={}",
             &filepath, i, map.len());
    Ok(())
}
