# Makefile for building anagram-phrases-httpd web service

.PHONY: all
all: test release

# If developing for this project:
.PHONY: developer
developer: deps

# This is done for you within Docker `rust` images for server instances.
# If you are deploying to a fresh workstation/laptop, start here.
# It is essentially https://www.rust-lang.org/en-US/install.html
# but modified with `-y` flag for full automation, plus some goodies.
.PHONY: deps
deps:
	curl -sS --output /tmp/install-rust.sh https://sh.rustup.rs
	/bin/bash /tmp/install-rust.sh -y
	rustup component add rls clippy rust-analysis rust-src
	cargo install cargo-tree
	cargo install cargo-audit

.PHONY: update
update:
	rustup self update
	rustup update stable

# By default the Rust installation modifies PATH within ~/.profile
# but you may want this set within ~/.bashrc instead:
.PHONY: dot-bashrc
dot-bashrc:
	grep -c '\.cargo/bin' ~/.bashrc && \
	  (echo '~/.bashrc already configured' && false) || true
	@echo 'PATH="$${PATH}:$${HOME}/.cargo/bin"' >> ~/.bashrc
	@echo 'if [ $$(which rustc) ]; then'  >> ~/.bashrc
	@echo -n '   export RUST_SRC_PATH=' >> ~/.bashrc
	@echo '"$$(rustc --print sysroot)/lib/rustlib/src/rust/src"' >> ~/.bashrc
	@echo '   export RUST_BACKTRACE=1' >> ~/.bashrc
	@echo 'fi' >> ~/.bashrc
	@echo "Next, manually run: . ~/.bashrc"

.PHONY: build
build:
	cargo build

# Using the help flag as a test in itself confirms the Clap config is valid
.PHONY: test
test:
	@echo "Running with --help to confirm clap .yaml config:"
	cargo run -- --help
	cargo test

.PHONY: audit
audit:
	cargo audit

.PHONY: release
release:
	cargo clean --release -p $(shell cargo pkgid)
	cargo build --release

# Build release for production using Docker with a Linux base image.
# (Avoid cross-compiling on macOS with Linux target, which is
# problematic due to third-party toolchains like crosstool-ng
# abandoning macOS as of 2018-11-26.)
# For most use cases, you only need the minimal "Docker Desktop for Mac".
# https://docs.docker.com/docker-for-mac/docker-toolbox/
# See also .dockerignore file.
.PHONY: docker
docker: docker-image
.PHONY: docker-image
docker-image:
	@which docker || \
	  echo 'Install: https://download.docker.com/mac/stable/Docker.dmg'
	docker build -t anagram-phrases-httpd .

.PHONY: docker-test
docker-test:
	docker run -it --rm anagram-phrases-httpd \
	  anagram-phrases-httpd -q=panic+moon \
	    --dict=/usr/share/dict/canadian-english-huge

.PHONY: docker-run
docker-run:
	@echo "Try: http://127.0.0.1:8080/anagrams?q=panic+moon&lang=EN"
	docker run -it --rm -p 127.0.0.1:8080:8080 anagram-phrases-httpd

.PHONY: docker-shell
docker-shell:
	docker run -it --rm -p 127.0.0.1:8080:8080 anagram-phrases-httpd bash

.PHONY: systemd
systemd: /usr/local/bin/anagram-phrases-httpd /etc/systemd/system/anagrams.service

/etc/systemd/system/anagrams.service: anagrams.service
	[ "$(shell uname -s)" = "Linux" ] || \
	  (echo "Is this Debian-based Linux?" && false)
	@echo "Invoking sudo to configure systemd"
	sudo cp anagrams.service /etc/systemd/system/
	sudo systemctl daemon-reload
	sudo systemctl enable anagrams.service
	sudo systemctl start anagrams.service
	sudo systemctl is-active anagrams.service

/usr/local/bin/anagram-phrases-httpd: target/release/anagram-phrases-httpd
	@echo "Invoking sudo to install an executable"
	sudo cp target/release/anagram-phrases-httpd /usr/local/bin/

.PHONY: clean
clean:
	cargo clean || true

.PHONY: dist-clean
dist-clean: clean
	find . -name '*~' -delete
	rm -f Cargo.lock
