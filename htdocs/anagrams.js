// anagrams.js - JavaScript v1.8 for extreme backwards compatibility in 2019
// https://gitlab.com/dpezely/anagram-phrases-httpd

// Enable logging to JS console or not:
var DEBUG = true;

// This assumes that public-facing URLs appear to all be on the same
// web server; e.g., using Nginx to route between static HTML versus
// upstream dynamic content.
var API_URI = "/api/v1/anagrams"; 

// Alternatively, you may use a completely separate host:
// var API_URI = "http://example.com/api/v1/anagrams"; 


// Event-handler for onLoad:
//
// Upon freshly loading the page, test for QUERY-STRING parameters
// within the URL, and if they exist, perform the async request to
// Anagrams API.  A successful response from the async call will then
// render from those JSON results.
//
// The flow described above is backwards with respect to the non-JS
// HTML FORM work-flow.  For those with JS disnabled, they must
// rely upon the browser to generically render JSON results.
function MAIN() {
    Log("MAIN()");
    if (process_query_strings(set_option)) {
        submit_async();
    }
    var node = document.getElementById('q');
    if (node) {
        node.focus();
    } else {
        Log('ERROR: Unable to find "q" query node!');
    }
}

function process_query_strings (param_dispatch_fn) {
    var qs = document.URL.match(/\?([^#]+)/);
    if (qs) {
	var query_strings = qs[1].match(/([^&]+)/g);
	if (query_strings.length>0) {
	    for (var i=0; i!=query_strings.length; ++i) {
		var pair = query_strings[i].match(/^([^=]+)=?(.*)$/);
		if (!pair) continue;
		var param = pair[1];
		var value = decodeURIComponent(pair[2]).replace('+', ' ');
		Log("Query-String param:", param, "value:", value);
		param_dispatch_fn.call(null, param, value);
	    }
	}
        return true;
    } else {
        Log('Query-Strings not supplied.');
        return false;
    }
}

function set_option(key, value) {
    var node = null;
    switch (key) {
    case 'q':
        document.title += ': ' + value;
        node = document.getElementById('q');
        if (node) {
            node.value = value;
        }
        break;
    case 'lang':
        node = document.getElementById('lang');
        if (node) {
            for (var child in node.children) {
                if (child.value == value) {
                    child.setAttribute('selected', true);
                    break;
                } else if (child.selected) {
                    child.removeAttribute('selected');
                }
            }
        }
        break;
    case 'max':
        node = document.getElementById('max');
        if (node) {
            node.value = value;
        }
        break;
    }
}

// Event-handler for FORM onSubmit:
//
// Extracts FORM key/value pairs, create new URL, advances browser
// page history to this new location, and then because of those query
// string parameters, performs async request to Anagrams API, and
// finally renders results.
//
// This function only deals with that flow up to and including
// assigning the new browser location.
function form_submit(form_element) {
    Log('do_submit()', form_element);
    var qs = form_data_to_query_strings(form_element);
    if (qs) {
        var base_href = document.URL.replace(/^([^?]*).*/,'$1');
        var url = base_href + '?' + qs;
        Log('Assign location=', url);
        location.assign(url);
    } else {
        Log('WARNING: ignoring attempt to submit FORM without value for query!');
    }
    return false;
}

// Convert FORM data to query-string parameters
function form_data_to_query_strings(form_element) {
    var params = [];
    var found_query = false;
    for (var i=0; i!=form_element.elements.length; ++i) {
        var item = form_element.elements[i];
        var value = item.value && item.value.trim();
        if (item.name && value) {
            if (item.name == 'q') {
                found_query = true;
            }
            params.push(encodeURIComponent(item.name) + '=' +
                        encodeURIComponent(item.value.trim()));
        }
    }
    if (found_query) {
        return params.join('&');
    } else {
        return null;
    }
}

function submit_async() {
    Log("submit()");
    var form_element = document.forms["anagrams"];
    if (form_element) {
        var node = document.getElementById('submit');
        if (node) {
            node.className += ' form__submit--inactive';
        } else {
            Log('ERROR: Unable to find "submit" button node!');
        }
        var qs = form_data_to_query_strings(form_element);
        if (qs) {
            status_message(true, 'Connecting to server...');
            if (API_URI[0] == '/') {
                var base_href = document.URL.replace(/^([^:]+:..[^\/]+)\/.*$/,'$1');
                base_href += API_URI;
                Log("base_href=", base_href, 'Query-Strings=', qs);
                send_async("GET", base_href + '?' + qs);
            } else {
                send_async("GET", API_URI + '?' + qs);
            }
        } else {
            Log('WARNING: attempt to submit without any query-strings!');
        }
    } else {
        Log('ERROR: unable to find "anagrams" form element!');
    }
    return false;
}

function send_async(method, uri) {
    Log("async()", method, uri);
    var request = null;
    // backwards compat with old browsers
    if (window.XMLHttpRequest) {
        request = new XMLHttpRequest();
    } else {
        request = new ActiveXObject("MSXML2.XMLHTTP.3.0");
    }
    try {
	request.open(method, uri, true);
    } catch (e) {
	server_unavailable();
	return;
    }
    request.onreadystatechange = function () {
        if (request && request.readyState!==undefined && request.readyState==4) {
	    if (request.status!==undefined && request.status==200) {
		if (request.responseText) {
		    success_handler(request.responseText);
		} else {
		    failure_handler(request.status);
                }
	    } else if (request.status!==undefined && request.status>=500) {
		server_unavailable();
	    } else {
                failure_handler(request.status);
            }
	    delete request;
	}
    };
    request.send(null);
}

// Primary rendering of JSON results
function success_handler(payload) {
    Log("success_handler()", payload.length);
    var data = JSON.parse(payload);
    var single_words = data["single-words"];
    var multiple_words = data["multiple-words"];
    Log("singles=", single_words.length, "multiples=", multiple_words.length);
    var node = document.getElementById('results');
    if (node) {
        clear_status(node);
        if (single_words.length == 0 && multiple_words.length == 0) {
            status_message(true, "No anagrams found.  Try another word.");
        } else {
            if (single_words.length > 0) {
                var singles = document.createElement('div');
                render_single_words(singles, single_words);
                singles.className = 'results__category';
                node.appendChild(singles);
            }
            if (multiple_words.length > 0) {
                var multiples = document.createElement('div');
                render_multiple_words(multiples, multiple_words);
                multiples.className = 'results__category';
                node.appendChild(multiples);
            }
        }
    } else {
        Log('ERROR: Unable to find "results" node!');
    }
    reenable_submit_button();
}

// SIDE-EFFECTS: appends children to `node`
function render_single_words(node, single_words) {
    var para = document.createElement('p');
    para.innerHTML = "Single Word<br>Anagrams:";
    node.appendChild(para);
    for (var i=0; i!=single_words.length; ++i) {
        var row = document.createElement('div');
        var word = single_words[i];
        row.innerHTML = word;
        if (i % 2 == 0) {
            row.className = "results__phrase results__row--even";
        } else {
            row.className = "results__phrase";
        }
        node.appendChild(row);
    }
}

// SIDE-EFFECTS: appends children to `node`
function render_multiple_words(node, multiple_words) {
    var para = document.createElement('p');
    para.innerHTML = "Multiple Word<br>Phrase Anagrams:";
    node.appendChild(para);
    for (var i=0; i!=multiple_words.length; ++i) {
        var row = document.createElement('div');
        var phrase = multiple_words[i];
        for (var j=0; j!=phrase.length; ++j) {
            var nested = document.createElement('div');
            var nested_words = [];
            var term = phrase[j];
            for (var k=0; k!=term.length; ++k) {
                nested_words.push(term[k]);
            }
            nested.innerHTML = nested_words.join('<br>');
            nested.className = "results__term";
            row.appendChild(nested);
        }
        if (i % 2 == 0) {
            row.className = "results__phrase results__row--even";
        } else {
            row.className = "results__phrase";
        }
        node.appendChild(row);
    }
}

function failure_handler(status) {
    Log("failure_handler()");
    switch (status) {
    case 0: status = "The server is too busy right now."; break;
    case 404: status = status + " / Not Found"; break;
    case 500: status = status + " / Internal Server Error"; break;
    }
    status_message(false, 'The Anagram API service returned:<br>' + status);
    reenable_submit_button();
    return false;
}

function server_unavailable() {
    Log("server_unavailable()");
    status_message(false, 'The Anagram API service is<br>currently unavailable');
    return false;
}

function status_message(nominal_behaviour, text) {
    Log('Status message:', text);
    var node = document.getElementById('results');
    if (node) {
        clear_status(node);
        var nested = document.createElement('p');
        nested.innerHTML = text;
        node.appendChild(nested);
        if (nominal_behaviour) {
            node.className = 'results--notify-ok'; // clobber existing
        } else {
            node.className = 'results--notify-warn'; // clobber existing
        }
    } else {
        Log('ERROR: Unable to find "results" node!');
    }
}

function clear_status(node) {
    node.className = '';
    while (node.firstChild) {
        var child = node.removeChild(node.firstChild);
        // FIXME: although this *should* be the "right thing to do",
        // it needs to be exhaustively tested across browsers:
        // delete(child);
    }
}

function reenable_submit_button() {
    var node = document.getElementById('submit');
    if (node) {
        node.className = node.className.replace(/ form__submit--inactive/, '');
    } else {
        Log('ERROR: Unable to find "submit" button node!');
    }
}    

// **********************************************************************
// Logging
if (DEBUG && window.console!==undefined && window.console.log!==undefined) {
    var Log = function () {
	if (/Firefox/.test(navigator.userAgent)) {
	    window.console.log.apply(console,arguments);
	} else {
	    if (arguments.length==1) {
		window.console.log(arguments[0]);
	    } else {
		var message = '';
		for (var i=0; i!=arguments.length; ++i) {
		    if (arguments[i]===undefined || 
			(arguments[i]!=0 && 
			 arguments[i]!='' &&
			 arguments[i]!=false) && !arguments[i]) {
			message += ' NULL';
		    } else {
			message += ' '+arguments[i].toString();
		    }
		}
		window.console.log(message);
	    }
	}
    };
} else {
    var Log = function () {};
}
